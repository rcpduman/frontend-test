import React, { Component } from 'react'
import axios from "axios";

const ContactContext = React.createContext();

const reducer = (state, action) => {
    switch (action.type) {
        case "DELETE_USER":

            return {
                ...state,
                contacts: state.contacts.filter(contact => action.payload !== contact.id)
            }
        case "ADD_USER":
            return {
                ...state,
                contacts: [...state.contacts, action.payload]
            }
        case "UPDATE_USER":
            return {
                ...state,
                contacts: state.contacts.map(contact => contact.id === action.payload.id ? action.payload : contact)
            }
        default:
            return state

    }
}


export class ContactProvider extends Component {
    state = {
        contacts: [],
        dispatch: action => {
            this.setState(state => reducer(state, action))
        }
    }
    componentDidMount = async () => {
        const response = await axios.get("http://localhost:3004/contacts")
        this.setState({
            contacts: response.data
        })
    }
    render() {
        return (
            <ContactContext.Provider value={this.state}>
                {this.props.children}
            </ContactContext.Provider>
        )
    }
}

const ContactConsumer = ContactContext.Consumer

export default ContactConsumer;