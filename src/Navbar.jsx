import React from 'react'
import { Link } from "react-router-dom";

export default function Navbar() {

  return (

    <nav className="navbar-nav navbar-expand-lg navbar-dark bg-dark mb-3 p-3">
      <ul className="navbar-nav ml-auto">
        <li className="nav-item active">
          <Link to = "/" className = "nav-link">Home</Link>
        </li>
        <li className="nav-item active">
          <Link to = "/add" className = "nav-link">Add Contact</Link>
       </li>
      </ul>
    </nav>

  )
}


