import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Enzyme, { mount } from 'enzyme';
import ContactConsumer  from "./context";
import ListContact from "./components/ListContact";
import AddContact from "./components/AddContact";
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

beforeEach(() => {
  jest.resetModules();
});

const addContentrWithContext = (contacts = {name:'Customer_1', email: 'customer@customer.com'}) => {
  
  jest.doMock('./ContactConsumer', () => {
    return {
      ContactConsumer: {
        Consumer: (props:any) => props.children(contacts)
      }
    }
  });
  
  return require('../src/components/AddContact').AddContact;
};

describe('<AddContact/>', () => {
  it('should return default contact', () => {
    // This will use the default context param since we pass nothing
    const component = mount(<AddContact />);
    component.find('button').simulate('click');
    expect(ContactConsumer.dispatch({ type: "ADD_USER", payload: addContentrWithContext}).call.length).toBe(1);
  });

});