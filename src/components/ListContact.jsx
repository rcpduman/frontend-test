import React, { Component } from 'react'
import axios from "axios";
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import ContactConsumer  from "../context";
import {Link} from "react-router-dom";

const URL = "http://localhost:3004/contacts";
export default class ListContact extends Component {

    constructor(props) {
        super(props);
        this.state = { contactList: [] }
    }

    onDeleteContact = async (dispatch,id, e) => {
        await axios.delete(URL + `/${id}`);
        dispatch({ type: "DELETE_USER", payload: id });
    }
    render() {
        return (
            <ContactConsumer>
                {
                    value => {
                        const { dispatch ,contacts} = value;
                        return (
                            <div>
                                {contacts.map((cl,index) =>
                                    <div key={index}>
                                        <div>
                                            <InputLabel htmlFor="component-simple"><b>ID :</b> </InputLabel>
                                            <InputLabel htmlFor="component-simple">{cl.id}</InputLabel>
                                        </div>
                                        <div>
                                            <InputLabel htmlFor="component-simple"><b>Name :</b> </InputLabel>
                                            <InputLabel htmlFor="component-simple">{cl.name} </InputLabel>
                                        </div>
                                        <div>
                                            <InputLabel htmlFor="component-simple"><b>Email : </b></InputLabel>
                                            <InputLabel htmlFor="component-simple">{cl.email}</InputLabel>
                                        </div>
                                        <div>
                                        <Link to = {`contact/${cl.id}`}>Update</Link>
                                        <Button variant="contained" onClick={this.onDeleteContact.bind(this, dispatch,cl.id)}  >
                                                Delete
                                        </Button>
                                        </div>
                                    </div>
                                )}

                            </div>
                        )
                    }
                }
            </ContactConsumer>

        )
    }
}
