import React, { Component } from 'react'
import axios from "axios";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import ContactConsumer from "../context";

const URL = "http://localhost:3004/contacts";
export default class UpdateContact extends Component {

    state = {
        name: "",
        email: "",
    }

    changeInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    componentDidMount = async () => {
        console.log(this.props.match.param);
        const { id } = this.props.match.params;
        const response = await axios.get(URL + `/${id}`);
        const { name, email } = response.data;

        this.setState({
            name,
            email
        });

    }
    validateForm = () => {
        const { name, email } = this.state;
        if (name === "" || email) {
            return false;
        }
        return true;

    }
    updateContact = async (dispatch, e) => {
        e.preventDefault();
        // Update Contact
        const {id, name, email } = this.state;
        const updatedContact = {
            name,
            email
        };
        const response = await axios.put(URL + `/${id}`, updatedContact);

        dispatch({ type: "UPDATE_USER", payload: response.data });

        // Redirect
        this.props.history.push("/");
    }
    render() {
        const { name, email } = this.state;
        return <ContactConsumer>
            {
                value => {
                    const { dispatch } = value;
                    return (
                        <div>
                            <form onSubmit={this.updateContact.bind(this, dispatch)}>
                                <div>
                                    <div>
                                        <InputLabel htmlFor="component-simple" ><b>Name</b></InputLabel>
                                        <Input id="name" value={name} name="name" onChange={this.changeInput} />
                                    </div>
                                    <div>
                                        <InputLabel htmlFor="component-simple"><b>Email</b></InputLabel>
                                        <Input id="email" value={email} name="email" onChange={this.changeInput} />
                                    </div>
                                    <Button variant="contained" type="submit"> Update
                                    </Button>
                                </div>
                            </form>
                        </div>
                    )
                }
            }
        </ContactConsumer>
    }
}
