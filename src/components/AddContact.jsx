import React, { Component } from 'react'
import axios from "axios";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import ContactConsumer from "../context";

const URL = "http://localhost:3004/contacts";
export default class AddContact extends Component {


    state = {
        id: "",
        name: "",
        email: ""
    }

    validateForm = () => {
        const { name, email } = this.state;
        if (name === "" || email === "") {
            return false;
        }
        return true;

    }
    changeInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    addContact = async (dispatch, e) => {
        e.preventDefault();
        const {id, name, email } = this.state;

        const newContact = {
            id,
            name,
            email
        }
        const response = await axios.post(URL, newContact);
        dispatch({ type: "ADD_USER", payload: response.data });
        this.props.history.push("/");
    }

    render() {
        const { name, email } = this.state;
        return (
            <ContactConsumer>
                {
                    value => {
                        const { dispatch } = value;
                        return (
                            <div>
                                <form onSubmit={this.addContact.bind(this, dispatch)}>
                                    <div>
                                        <div>
                                            <InputLabel htmlFor="component-simple" ><b>Name</b></InputLabel>
                                            <Input id="name" value={name} name="name" onChange={this.changeInput} />
                                        </div>
                                        <div>
                                            <InputLabel htmlFor="component-simple"><b>Email</b></InputLabel>
                                            <Input id="email" value={email} name="email" onChange={this.changeInput} />
                                        </div>
                                        <div>

                                        </div>
                                        <Button variant="contained" type="submit">
                                            Save
                                        </Button>
                                    </div>
                                </form>
                            </div>
                        )
                    }}</ContactConsumer>
        )
    }
}
