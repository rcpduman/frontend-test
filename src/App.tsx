import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";
import AddContact from "./components/AddContact";
import ListContact from "./components/ListContact";
import UpdateContact from "./components/UpdateContact";
import Navbar from "../src/Navbar";

class App extends Component {
  render() {
    return (
      <Router>
        <Navbar />
        <div className="App">
          <Switch>
            <Route exact path="/" component={ListContact} />
            <Route exact path="/add" component={AddContact} />
            <Route exact path="/contact/:id" component={UpdateContact} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
